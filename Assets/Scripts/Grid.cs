﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Grid : MonoBehaviour {

    [HideInInspector]
    public List<Node> path;
    public LayerMask unwalkableMask;
    public Vector2 gridWorldSize;
    // how much each individual node cover;
    public float nodeRadius;

    Node[,] grid;
    float nodeDiameter;
    int gridSizeX, gridSizeY;

    public int MaxSize {
        get {
            return gridSizeX * gridSizeY;
        }
    }

    void Start() {
        nodeDiameter = nodeRadius * 2;
        gridSizeX = Mathf.RoundToInt(gridWorldSize.x / nodeDiameter);
        gridSizeY = Mathf.RoundToInt(gridWorldSize.y / nodeDiameter);
        CreateGrid();
    }

    // create the grid of nodes
    void CreateGrid() {
        grid = new Node[gridSizeX, gridSizeY];
        // bottom left corner of the grid
        Vector3 worldBottomLeft = transform.position - Vector3.right * gridWorldSize.x / 2 - Vector3.forward * gridWorldSize.y / 2;
        for (int x = 0; x < gridSizeX; x++) {
            for (int y = 0; y < gridSizeY; y++) {
                // coordinate of the point
                Vector3 worldPoint = worldBottomLeft + Vector3.right * (x * nodeDiameter + nodeRadius) + Vector3.forward * (y * nodeDiameter + nodeRadius);
                // check if is walkable
                bool walkable = !Physics.CheckSphere(worldPoint, nodeRadius, unwalkableMask);
                // add the node
                grid[x, y] = new Node(walkable, worldPoint, x, y);
            }
        }
    }

    // return all the neighbours 
    public List<Node> GetNeighbours(Node node) {
        List<Node> neighbours = new List<Node>();
        for (int x = -1; x <= 1; x++) {
			for (int y = -1; y <= 1; y++) {
                if (x == 0 && y == 0) {
                    continue;
                }
                // check if is inside the grid
                int checkX = node.gridX + x;
                int checkY = node.gridY + y;
                if (checkX >= 0 && checkX < gridSizeX && checkY >= 0 && checkY < gridSizeY) {
                    // Add to the neighbours
                    neighbours.Add(grid[checkX, checkY]);
                }
            }
        }
        return neighbours;
    }

    public Node GetNodeFromWorldPoint(Vector3 worldPosition) {
        // fit the grid
        float percentX = (worldPosition.x + gridWorldSize.x / 2f) / gridWorldSize.x;
        float percentY = (worldPosition.z + gridWorldSize.y / 2f) / gridWorldSize.y;
        // if is outside the grid it don't give us an invalid percent
        percentX = Mathf.Clamp01(percentX);
        percentY = Mathf.Clamp01(percentY);
        // search in the grid position
        int x = Mathf.RoundToInt((gridSizeX-1) * percentX);
        int y = Mathf.RoundToInt((gridSizeY-1) * percentY);
        return grid[x, y];
    }

    // draw the grid
    void OnDrawGizmos() {
        // the y axes rapresent the z axes in 3D space
        Gizmos.DrawWireCube(transform.position, new Vector3(gridWorldSize.x, 1f, gridWorldSize.y));
        if (grid != null) {
            foreach (Node node in grid) {
                Gizmos.color = (node.walkable) ? Color.blue : Color.white;
                if (path != null) {
                    if (path.Contains(node)) {
                        Gizmos.color = Color.black;
                    }
                }
                Gizmos.DrawCube(node.worldPosition, Vector3.one * (nodeDiameter - 0.1f));
            }
        }
    }
}

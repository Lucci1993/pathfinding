﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pathfinding : MonoBehaviour {

    public Transform seeker, target;

    Grid grid;

    void Awake() {
        grid = GetComponent<Grid>();
    }

	void Update() {
        FindPath(seeker.position, target.position);
	}

    // find a path from a start to a target point
    void FindPath(Vector3 startPosition, Vector3 targetPosition) {
        Node startNode = grid.GetNodeFromWorldPoint(startPosition);
        Node targetNode = grid.GetNodeFromWorldPoint(targetPosition);

        Heap<Node> openSet = new Heap<Node>(grid.MaxSize);
        HashSet<Node> closedSet = new HashSet<Node>();
        openSet.Add(startNode);

        while (openSet.Count > 0) {
            Node currentNode = openSet.RemoveFirst();
            closedSet.Add(currentNode);
            // arrive
            if (currentNode == targetNode) {
                RetracePath(startNode, targetNode);
                return;
            }
            foreach (Node neighbour in grid.GetNeighbours(currentNode)) {
                // if the neighbour is not ok, check another
                if (!neighbour.walkable || closedSet.Contains(neighbour)) {
                    continue;
                }
                int newMovementCostToNeighbour = currentNode.gCost + GetDistance(currentNode, neighbour);
                // if is shorter or not in open set
                if (newMovementCostToNeighbour < neighbour.gCost || !openSet.Contains(neighbour)) {
                    neighbour.gCost = newMovementCostToNeighbour;
                    neighbour.hCost = GetDistance(neighbour, targetNode);
                    neighbour.parent = currentNode;
                }
                // check if is not in the open set and ad to it
                if (!openSet.Contains(neighbour)) {
                    openSet.Add(neighbour);
                }
                else {
                    openSet.UpdateItem(neighbour);
                }
            }
        }
    }

    // get distance between two node
    int GetDistance(Node nodeA, Node nodeB) {
        int distX = Mathf.Abs(nodeA.gridX - nodeB.gridX);
        int distY = Mathf.Abs(nodeA.gridY - nodeB.gridY);
        if (distX > distY) {
            return 14 * distY + 10 * (distY - distY);
        }
        return 14 * distX + 10 * (distY - distX);
    }

    // retrive the path searched from parent nodes
    void RetracePath(Node startNode, Node endNode) {
        List<Node> path = new List<Node>();
        Node currentNode = endNode;
        while (currentNode != startNode) {
            path.Add(currentNode);
            currentNode = currentNode.parent;
        }
        path.Reverse();
        // JUST TO TEST
        grid.path = path;
    }
}
